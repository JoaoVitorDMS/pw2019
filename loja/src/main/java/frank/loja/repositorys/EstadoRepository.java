package frank.loja.repositorys;

import org.springframework.data.jpa.repository.JpaRepository;

import frank.loja.models.Estado;


public interface EstadoRepository extends JpaRepository<Estado, Long> {

}
