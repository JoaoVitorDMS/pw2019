package frank.loja.repositorys;

import org.springframework.data.jpa.repository.JpaRepository;

import frank.loja.models.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Long> {

}
