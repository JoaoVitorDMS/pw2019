package frank.loja.repositorys;

import org.springframework.data.jpa.repository.JpaRepository;

import frank.loja.models.Funcionario;


public interface FuncionarioRepository extends JpaRepository<Funcionario, Long> {

}
