package frank.loja.repositorys;

import org.springframework.data.jpa.repository.JpaRepository;

import frank.loja.models.Compra;

public interface CompraRepository extends JpaRepository<Compra, Long> {

}
