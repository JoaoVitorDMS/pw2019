package frank.loja.repositorys;

import org.springframework.data.jpa.repository.JpaRepository;

import frank.loja.models.Papel;

public interface PapelRepository extends JpaRepository<Papel, Long> {

}
