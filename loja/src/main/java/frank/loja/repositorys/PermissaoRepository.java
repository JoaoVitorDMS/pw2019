package frank.loja.repositorys;

import org.springframework.data.jpa.repository.JpaRepository;

import frank.loja.models.Permissao;

public interface PermissaoRepository extends JpaRepository<Permissao, Long>{

}
