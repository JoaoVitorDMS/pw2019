package frank.loja.repositorys;

import org.springframework.data.jpa.repository.JpaRepository;

import frank.loja.models.Cidade;


public interface CidadeRepository extends JpaRepository<Cidade, Long> {

}
