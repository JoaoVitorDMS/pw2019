package frank.loja.repositorys;

import org.springframework.data.jpa.repository.JpaRepository;

import frank.loja.models.ItensCompra;

public interface ItensCompraRepository extends JpaRepository<ItensCompra, Long> {

}
