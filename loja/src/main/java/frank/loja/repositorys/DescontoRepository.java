package frank.loja.repositorys;

import org.springframework.data.jpa.repository.JpaRepository;

import frank.loja.models.Desconto;

public interface DescontoRepository extends JpaRepository<Desconto, Long> {

}
