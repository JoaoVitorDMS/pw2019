package frank.loja.repositorys;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import frank.loja.models.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {

	@Query("select e from Produto e where e.nomeProd like %?1%")
	List<Produto> buscarNome(String nomeProd);

	@Query("select e from Produto e where e.marca.id=?1")
	List<Produto> buscarProdutoMarca(Long marcaProd);
	
	@Query("select e from Produto e where e.id=?1")
	List<Produto> buscarProdutoId(Long id);
}
