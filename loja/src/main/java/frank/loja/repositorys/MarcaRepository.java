package frank.loja.repositorys;

import org.springframework.data.jpa.repository.JpaRepository;
import frank.loja.models.Marca;

public interface MarcaRepository extends JpaRepository<Marca, Long> {

}
