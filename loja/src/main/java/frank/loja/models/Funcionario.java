package frank.loja.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="funcionario")
public class Funcionario implements Serializable {
	public Funcionario() {
		super();
	}

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(nullable = false, length = 255)
	@NotEmpty(message = "Nome do funcionario é obrigatório")
	private String nome;
	@Column(nullable = false, length = 255)
	@NotEmpty(message = "Data de aniversário é obrigatório")
	private String dtnasc;
	@Column(nullable = false, length = 11)
	@NotEmpty(message = "CPF do funcionario é obrigatório")
	private String cpf;
	@Column(nullable = false)
	@NotEmpty(message = "RG do funcionario é obrigatório")
	private String rg;
	@Column(nullable = false, length = 14)
	@NotEmpty(message = "Telefone do funcionario é obrigatório")
	private String telefone;
	@Column(nullable = false, length = 255)
	@NotEmpty(message = "Email do funcionario é obrigatório")
	private String email;
	@Column(nullable = false, length = 255)
	@NotEmpty(message = "Endereço do funcionario é obrigatório")
	private String endereco;
	@Column(nullable = false, length = 255)
	@NotEmpty(message = "Senha é obrigatório")
	private String senha;
	@ManyToOne
	private Cidade cidade;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDtnasc() {
		return dtnasc;
	}

	public void setDtnasc(String dtnasc) {
		this.dtnasc = dtnasc;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public static Long getSerialversionuid() {
		return serialVersionUID;
	}

}
