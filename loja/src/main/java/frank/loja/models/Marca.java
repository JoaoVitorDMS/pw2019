package frank.loja.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
@Entity
public class Marca implements Serializable {
	public Marca() {
		super();
	}

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(nullable = false)
	@NotEmpty(message = "Nome da marca é obrigatória")
	private String nomeMarc;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNomeMarc() {
		return nomeMarc;
	}
	public void setNomeMarc(String nomeMarc) {
		this.nomeMarc = nomeMarc;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
