package frank.loja.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
@Entity
public class Desconto implements Serializable {
	public Desconto() {
		super();
	}

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@NotEmpty(message="Nome do desconto é obrigatório")
	@Column(nullable = false)
	private String nomeDesc;
	@Column(nullable = false, length = 255)
	@NotEmpty(message = "Data do cadastro é obrigatório")
	private String dataDesc;
	@Column(nullable=false)
	private int valorDesc;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNomeDesc() {
		return nomeDesc;
	}
	public void setNomeDesc(String nomeDesc) {
		this.nomeDesc = nomeDesc;
	}

	public String getDataDesc() {
		return dataDesc;
	}
	public void setDataDesc(String dataDesc) {
		this.dataDesc = dataDesc;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getValorDesc() {
		return valorDesc;
	}
	public void setValorDesc(int valorDesc) {
		this.valorDesc = valorDesc;
	}
	
}
