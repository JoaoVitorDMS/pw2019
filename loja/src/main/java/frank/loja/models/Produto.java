package frank.loja.models;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.ManyToAny;
import org.springframework.format.annotation.DateTimeFormat;
@Entity
public class Produto implements Serializable {
	public Produto() {
		super();
	}

	private static final long serialVersionUID =1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(nullable = false)
	@NotEmpty(message="Nome do produto é obrigatório")
	private String nomeProd;
	@Column(nullable = false)
	@NotEmpty(message="A descrição do produto é obrigatória")
	private String descricao;
	@Column(nullable= false)
	private Double precoProd;
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date dataProd;
	@Column(nullable = false)
	@NotEmpty(message= "URL da imagem é obrigatória")
	private String imagem;
	@ManyToOne
	private Marca marca;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNomeProd() {
		return nomeProd;
	}
	public void setNomeProd(String nomeProd) {
		this.nomeProd = nomeProd;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Double getPrecoProd() {
		return precoProd;
	}
	public void setPrecoProd(Double precoProd) {
		this.precoProd = precoProd;
	}
	public Date getDataProd() {
		return dataProd;
	}
	public void setDataProd(Date dataProd) {
		this.dataProd = dataProd;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Marca getMarca() {
		return marca;
	}
	public void setMarca(Marca marca) {
		this.marca = marca;
	}
	
	

}
