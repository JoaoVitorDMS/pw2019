package frank.loja.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import frank.loja.models.Estado;
import frank.loja.repositorys.EstadoRepository;

@Controller
public class EstadoController {
	@Autowired
	EstadoRepository repository;
	@GetMapping("administrativo/estadoForm")
	public ModelAndView findAll() {
		ModelAndView mv = new ModelAndView("administrativo/estadoForm");
		mv.addObject("estados", repository.findAll());
		return mv;
	    }
	@RequestMapping("administrativo/estadoAdd")
	public ModelAndView add(Estado estado){
		ModelAndView mv = new ModelAndView("administrativo/estadoAdd");
		mv.addObject("estado",estado);
		return mv;
	    }
	@RequestMapping("administrativo/editarEstado/{id}")
	public ModelAndView edit(@PathVariable ("id") Long id) {
		Optional<Estado> estado = repository.findById(id);
		Estado esta = estado.get();
		return add(esta);
		
	    }
	@RequestMapping("administrativo/removerEstado/{id}")
	public ModelAndView delete(@PathVariable ("id") Long id) {
		Optional<Estado> estado = repository.findById(id);		
		Estado esta = estado.get();
		repository.delete(esta);
		return findAll();
	    }
	@RequestMapping(value = "administrativo/salvarEstado", method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE ,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Estado salvar(@RequestBody Estado estado, BindingResult result) {
		repository.saveAndFlush(estado);
		return estado;
	   }
	}