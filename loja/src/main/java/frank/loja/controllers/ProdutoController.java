package frank.loja.controllers;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import frank.loja.models.Marca;
import frank.loja.models.Produto;
import frank.loja.repositorys.MarcaRepository;
import frank.loja.repositorys.ProdutoRepository;

@RestController
public class ProdutoController {
	@Autowired
	private ProdutoRepository pro;
	@Autowired
	private MarcaRepository mar;

	@RequestMapping("administrativo/produtoForm")
	public ModelAndView findAll() {
		ModelAndView mv = new ModelAndView("administrativo/produtoForm");
		mv.addObject("produtos", pro.findAll());
		return mv;
	}

	@RequestMapping("administrativo/produtoAdd")
	public ModelAndView add(Produto produto) {
		ModelAndView mv = new ModelAndView("administrativo/produtoAdd");
		mv.addObject("produto", produto);
		List<Marca> listMar = mar.findAll();
		mv.addObject("marcas", listMar);
		return mv;
	}

	@RequestMapping("/produtoNome")
	public ModelAndView buscarPorNome(String nomeProd) {
		ModelAndView mv = new ModelAndView("usuario/category");
		mv.addObject("produtos", pro.buscarNome(nomeProd));
		return mv;
	}


	@RequestMapping("administrativo/produtoEditar/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		Optional<Produto> pr = pro.findById(id);
		Produto p = pr.get();
		return add(p);
	}

	@RequestMapping("administrativo/produtoRemover/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		Optional<Produto> p = pro.findById(id);
		Produto pr = p.get();
		pro.delete(pr);
		return findAll();
	}

	@RequestMapping("administrativo/produtoSalvar")
	public ModelAndView save(@Valid Produto produto, BindingResult result) {
		if (result.hasErrors()) {
			return add(produto);
		}
		pro.saveAndFlush(produto);
		return findAll();
	}
}
