package frank.loja.controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import frank.loja.models.Cidade;
import frank.loja.models.Funcionario;
import frank.loja.repositorys.CidadeRepository;
import frank.loja.repositorys.FuncionarioRepository;

@Controller
public class FuncionarioController {
	@Autowired
	private FuncionarioRepository repository;
	@Autowired
	private CidadeRepository cid;
	@RequestMapping("administrativo/funcionarioForm")
	public ModelAndView findAll() {
		ModelAndView mv = new ModelAndView("administrativo/funcionarioForm");
		mv.addObject("funcionarios", repository.findAll());
		return mv;
	}
	@RequestMapping("administrativo/funcionarioAdd")
	public ModelAndView add(Funcionario funcionario) {
		ModelAndView mv = new ModelAndView("administrativo/funcionarioAdd");
		mv.addObject("funcionario", funcionario);
		List<Cidade> listCidade = cid.findAll();
		mv.addObject("cidades",listCidade);
		return mv;
	}
	@RequestMapping("administrativo/funcionarioEditar/{id}")
	public ModelAndView edit(@PathVariable ("id") Long id) {
		Optional<Funcionario> funcionario = repository.findById(id);
		Funcionario func = funcionario.get();
		return add(func);
	}
	@RequestMapping("administrativo/funcionarioRemover/{id}")
	public ModelAndView delete(@PathVariable ("id") Long id) {
		Optional<Funcionario> funcionario = repository.findById(id);
		Funcionario func = funcionario.get();
		repository.delete(func);
		return findAll();
	}
	@RequestMapping("administrativo/funcionarioSalvar")
	public ModelAndView save(@Valid Funcionario funcionario, BindingResult result) {
		if(result.hasErrors()) {
			return add(funcionario);
		}
		funcionario.setSenha(new BCryptPasswordEncoder().encode(funcionario.getSenha()));
		repository.saveAndFlush(funcionario);
		return findAll();
	}
}
