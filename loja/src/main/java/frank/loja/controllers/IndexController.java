package frank.loja.controllers;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import frank.loja.models.Marca;
import frank.loja.models.Produto;
import frank.loja.repositorys.MarcaRepository;
import frank.loja.repositorys.ProdutoRepository;

@Controller
public class IndexController {
	@Autowired
	private ProdutoRepository pr;
	@Autowired
	private MarcaRepository mar;

	List<Marca> listMar = new ArrayList<Marca>();

	@PostConstruct
	public void iniciar() {
		listMar = mar.findAll();
	}

	@RequestMapping("administrativo/index")
	public String index() {
		return "administrativo/index";
	}

	@RequestMapping("/")
	public String indexUser() {
		return "usuario/index";
	}

	@RequestMapping("/contact")
	public String contato() {
		return "usuario/contact";
	}

	@RequestMapping("/shop")
	public ModelAndView category() {
		ModelAndView mv = new ModelAndView("usuario/category");
		List<Produto> listprod = pr.findAll();
		mv.addObject("produtos", listprod);

		mv.addObject("marcas", listMar);
		return mv;
	}

	@RequestMapping("/shop/{id}")
	public ModelAndView buscarPorMarca(@PathVariable("id") Long id) {

		ModelAndView mv = new ModelAndView("usuario/category");
		List<Produto> listprod = pr.buscarProdutoMarca(id);
		mv.addObject("produtos", listprod);
		mv.addObject("marcas", listMar);
		return mv;
	}

	@RequestMapping("/tracking")
	public String tracking() {
		return "usuario/tracking";
	}
	@RequestMapping("/negadoAdmin")
	public String nega() {
		return "/negadoAdmin";
	}
	@RequestMapping("/negadoCliente")
	public String negado() {
		return "/negadoCliente";
	}
	@RequestMapping("/login")
	public String login() {
		return "/login";
	}
	@RequestMapping("/loginCliente")
	public String log() {
		return "usuario/loginCliente";
	}
	@RequestMapping("/confirmation")
	public String confirm() {
		return "usuario/confirmation";
	}

	@RequestMapping("/checkout")
	public String checagem() {
		return "usuario/checkout";
	}

	@RequestMapping("/single-blog")
	public String singleBlog() {
		return "usuario/single-blog";
	}

	@RequestMapping("/single-product/{id}")
	public ModelAndView singleProdu(@PathVariable("id") Long id) {
		ModelAndView mv = new ModelAndView("usuario/single-product");
		List<Produto> prod = pr.buscarProdutoId(id);
		mv.addObject("produtos", prod);
		mv.addObject("marcas", listMar);
		return mv;
	}

	@RequestMapping("/single-product")
	public ModelAndView single() {
		ModelAndView mv = new ModelAndView("usuario/single-product");
		List<Produto> pro = pr.findAll();
		mv.addObject("produtos", pro);
		return mv;
	}

	@RequestMapping("/blog")
	public String blog() {
		return "usuario/blog";
	}

}