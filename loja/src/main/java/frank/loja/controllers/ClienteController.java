package frank.loja.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import frank.loja.models.Cliente;
import frank.loja.repositorys.CidadeRepository;
import frank.loja.repositorys.ClienteRepository;

@Controller
public class ClienteController {
@Autowired
private ClienteRepository clie;
@Autowired
private CidadeRepository repo;
@RequestMapping("/register")
public ModelAndView findAll(Cliente cliente) {
	ModelAndView mv = new ModelAndView("usuario/register");
	mv.addObject("cliente",cliente);
	mv.addObject("cidades",repo.findAll());
	return mv;
}
@RequestMapping("/registroEditar/{id}")
public ModelAndView edit(@PathVariable ("id") Long id) {
	Optional<Cliente> cliente = clie.findById(id);
	return findAll(cliente.get());
}
@RequestMapping("/cadastroSalvar")
public ModelAndView save(@Valid Cliente cliente, BindingResult result) {
	if(result.hasErrors()) {
		return findAll(cliente);
	}
	cliente.setSenha(new BCryptPasswordEncoder().encode(cliente.getSenha()));
	clie.saveAndFlush(cliente);
	return findAll(new Cliente());
}
}

