package frank.loja.controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import frank.loja.models.Funcionario;
import frank.loja.models.Papel;
import frank.loja.models.Permissao;
import frank.loja.repositorys.FuncionarioRepository;
import frank.loja.repositorys.PapelRepository;
import frank.loja.repositorys.PermissaoRepository;

@Controller
public class PermissaoController {
	@Autowired
	private PermissaoRepository carg;
	@Autowired
	private FuncionarioRepository repo;
	@Autowired
	private PapelRepository pa;
	@RequestMapping("administrativo/permissaoForm")
	public ModelAndView findAll() {
		ModelAndView mv = new ModelAndView("administrativo/permissaoForm");
		mv.addObject("permissoes", carg.findAll());
		return mv;
	}

	@RequestMapping("administrativo/permissaoAdd")
	public ModelAndView add(Permissao permi) {
		ModelAndView mv = new ModelAndView("administrativo/permissaoAdd");
		mv.addObject("permi", permi);
		List<Funcionario> listFunc = repo.findAll();
		mv.addObject("funcionarios",listFunc);
		List<Papel> listPapel = pa.findAll();
		mv.addObject("papeis",listPapel);
		return mv;
	}

	@RequestMapping("administrativo/permissaoEditar/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		Optional<Permissao> permi = carg.findById(id);
		Permissao ca = permi.get();
		return add(ca);
	}

	@RequestMapping("administrativo/permissaoRemover/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		Optional<Permissao> permi = carg.findById(id);
		Permissao ca = permi.get();
		carg.delete(ca);
		return findAll();
	}

	@RequestMapping("administrativo/permissaoSalvar")
	public ModelAndView save(@Valid Permissao permi, BindingResult result) {
		if (result.hasErrors()) {
			return add(permi);
		}
		carg.saveAndFlush(permi);
		return findAll();
	}
}
