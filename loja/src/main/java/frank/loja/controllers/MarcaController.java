package frank.loja.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import frank.loja.models.Marca;
import frank.loja.repositorys.MarcaRepository;

@Controller
public class MarcaController {
	@Autowired
	private MarcaRepository mar;
@RequestMapping("administrativo/marcaForm")
public ModelAndView findAll() {
	ModelAndView mv = new ModelAndView("administrativo/marcaForm");
	mv.addObject("marcas", mar.findAll());
	return mv;
   }
@RequestMapping("administrativo/marcaAdd")
public ModelAndView add(Marca marca) {
	ModelAndView mv = new ModelAndView("administrativo/marcaAdd");
	mv.addObject("marca",marca);
	return mv;
    }

@RequestMapping("administrativo/marcaEditar/{id}")
public ModelAndView edit(@PathVariable ("id")Long id) {
	Optional<Marca> listMarc = mar.findById(id);
	Marca ma = listMarc.get();
	return add(ma);
   }
@RequestMapping("administrativo/marcaRemover/{id}")
public ModelAndView delete(@PathVariable ("id")Long id) {
	Optional<Marca> listMarc = mar.findById(id);
	Marca ma = listMarc.get();
	mar.delete(ma);
	return findAll();
  }
@RequestMapping("administrativo/marcaSalvar")
public ModelAndView save(@Valid Marca marca, BindingResult result) {
	if(result.hasErrors()) {
		return add(marca);
	}
	mar.saveAndFlush(marca);
	return findAll();
   }
}
