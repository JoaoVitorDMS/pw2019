package frank.loja.controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import frank.loja.models.Cidade;
import frank.loja.models.Estado;
import frank.loja.repositorys.CidadeRepository;
import frank.loja.repositorys.EstadoRepository;

@Controller
public class CidadeController {
	@Autowired
	CidadeRepository repository;
	@Autowired
	EstadoRepository EstaRepository;
	@RequestMapping("administrativo/cidadeForm")
	public ModelAndView findAll() {
		ModelAndView mv = new ModelAndView("administrativo/cidadeForm");
		mv.addObject("cidades", repository.findAll());
		return mv;
	}
	@RequestMapping("administrativo/cidadeAdd")
	public ModelAndView add(Cidade cidade) {
		ModelAndView mv = new ModelAndView("administrativo/cidadeAdd");
		mv.addObject("cidade",cidade);
		List<Estado> listEstado = EstaRepository.findAll();
		mv.addObject("estados", listEstado);
		return mv;
	}
	@RequestMapping("administrativo/cidadeEditar/{id}")
	public ModelAndView edit(@PathVariable ("id") Long id) {
		Optional<Cidade> cidade = repository.findById(id);
		Cidade cid = cidade.get();
		return add(cid);
	}
	@RequestMapping("administrativo/cidadeRemover/{id}")
	public ModelAndView delete(@PathVariable ("id") Long id) {
		Optional<Cidade> cidade = repository.findById(id);
		Cidade cid = cidade.get();
		repository.delete(cid);
		return findAll();
	}
	@RequestMapping("administrativo/cidadeSalvar")
	public ModelAndView save(@Valid Cidade cidade, BindingResult result) {
		if(result.hasErrors()) {
			return add(cidade);
		}
		repository.saveAndFlush(cidade);
		return findAll();
	}
}
