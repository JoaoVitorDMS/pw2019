package frank.loja.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import frank.loja.models.Desconto;
import frank.loja.repositorys.DescontoRepository;

@Controller
public class DescontoController {
@Autowired
private DescontoRepository desc;
@RequestMapping("administrativo/descontoForm")
public ModelAndView findAll() {
	ModelAndView mv = new ModelAndView("administrativo/descontoForm");
	mv.addObject("descontos",desc.findAll());
	return mv;
}
@RequestMapping("administrativo/descontoAdd")
public ModelAndView add(Desconto desconto) {
	ModelAndView mv = new ModelAndView("administrativo/descontoAdd");
	mv.addObject("desconto", desconto);
	return mv;
}
@RequestMapping("administrativo/descontoEditar/{id}")
public ModelAndView edit(@PathVariable ("id") Long id) {
	Optional<Desconto> conto = desc.findById(id);
	Desconto desconto = conto.get();
	return add(desconto);
}
@RequestMapping("administrativo/descontoRemover/{id}")
public ModelAndView delete(@PathVariable ("id") Long id) {
	Optional<Desconto> conto = desc.findById(id);
	Desconto desconto = conto.get();
	desc.delete(desconto);
	return findAll();
}
@RequestMapping("administrativo/descontoSalvar")
public ModelAndView save(@Valid Desconto desconto, BindingResult result) {
	if(result.hasErrors()) {
		return add(desconto);
	}
	desc.saveAndFlush(desconto);
	return findAll();
}
}

