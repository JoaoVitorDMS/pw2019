package frank.loja.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import frank.loja.models.Papel;
import frank.loja.repositorys.PapelRepository;

@Controller
public class PapelController {
@Autowired
private PapelRepository carg;
@RequestMapping("administrativo/papelForm")
public ModelAndView findAll() {
	ModelAndView mv = new ModelAndView("administrativo/papelForm");
			mv.addObject("papeis", carg.findAll());
	return mv;
}
@RequestMapping("administrativo/papelAdd")
public ModelAndView add(Papel papel) {
	ModelAndView mv = new ModelAndView("administrativo/papelAdd");
	mv.addObject("papel", papel);
	return mv;
}
@RequestMapping("administrativo/papelEditar/{id}")
public ModelAndView edit(@PathVariable ("id") Long id) {
	Optional<Papel> cargo = carg.findById(id);
	Papel ca = cargo.get();
	return add(ca);
}
@RequestMapping("administrativo/papelRemover/{id}")
public ModelAndView delete(@PathVariable ("id") Long id) {
	Optional<Papel> cargo = carg.findById(id);
	Papel ca = cargo.get();
	carg.delete(ca);
	return findAll();
}
@RequestMapping("administrativo/papelSalvar")
public ModelAndView save(@Valid Papel cargo, BindingResult result) {
	if(result.hasErrors()) {
		return add(cargo);
	}
	carg.saveAndFlush(cargo);
	return findAll();
   }
}
