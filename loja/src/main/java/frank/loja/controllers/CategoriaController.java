package frank.loja.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import frank.loja.models.Categoria;
import frank.loja.repositorys.CategoriaRepository;

@Controller
public class CategoriaController {
	@Autowired
	private CategoriaRepository cate;
	@Autowired
@RequestMapping("administrativo/categoriaForm")
public ModelAndView findAll() {
	ModelAndView mv = new ModelAndView("administrativo/categoriaForm");
	mv.addObject("categorias", cate.findAll());
	return mv;
     }
@RequestMapping("administrativo/categoriaAdd")
public ModelAndView add(Categoria categoria) {
	ModelAndView mv = new ModelAndView("administrativo/categoriaAdd");
	mv.addObject("categoria", categoria);
	return mv;
    }
@RequestMapping("administrativo/categoriaEditar/{id}")
public ModelAndView edit(@PathVariable ("id") Long id) {
	Optional<Categoria> listCategoria = cate.findById(id);
	Categoria ca = listCategoria.get();
	return add(ca);
   }
@RequestMapping("administrativo/categoriaRemover/{id}")
public ModelAndView delete(@PathVariable ("id") Long id) {
	Optional<Categoria> listCategoria = cate.findById(id);
	Categoria ca = listCategoria.get();
	cate.delete(ca);
	return findAll();
   }
@RequestMapping("administrativo/categoriaSalvar")
public ModelAndView save(@Valid Categoria categoria, BindingResult result) {
	if(result.hasErrors()) {
		return add(categoria);
	}
	cate.saveAndFlush(categoria);
	return findAll();
	
}
}
